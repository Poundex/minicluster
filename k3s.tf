

module "k3s" {
  depends_on = [local_file.prkey, module.k8s_server, module.vpc]

#  source  = "xunleii/k3s/module"
  source = "github.com/xunleii/terraform-module-k3s"
  k3s_version = "v1.24.3+k3s1"

  global_flags = [
    "--tls-san ${local.k8s_server_domain}",
    "--write-kubeconfig-mode 644"
  ]

  managed_fields = ["label", "taint"]
  generate_ca_certificates = false
  use_sudo = false

  servers = {
    server-one = {
      ip         = module.k8s_server.private_ip
      connection = {
        host = aws_eip.serverip.public_ip
        user = "ubuntu"
        private_key = tls_private_key.keypair.private_key_pem
      }
      flags = ["--disable traefik", "--disable local-storage"]
      labels = {"node.kubernetes.io/type" = "master"}
      taints = {"node.k3s.io/type" = "server:NoSchedule"}
    }
  }
}

resource "null_resource" "fetch_kubeconfig" {
  depends_on = [module.k3s]
  triggers = {
    everytime = md5(timestamp())
  }

  provisioner "remote-exec" {
    inline = ["sh -c -- \"sudo cp /etc/rancher/k3s/k3s.yaml /tmp/kubeconfig; sudo chmod a+r /tmp/kubeconfig\""]
  }

  provisioner "local-exec" {
    command = "scp -i ${local_file.prkey.filename} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${aws_eip.serverip.public_ip}:/tmp/kubeconfig kubeconfig"
  }

  provisioner "local-exec" {
    command = "sed \"s/127.0.0.1/${local.k8s_server_domain}/g\" -i kubeconfig"
  }

  provisioner "remote-exec" {
    inline = ["sh -c -- \"sudo rm -f /tmp/kubeconfig\""]
  }

  connection {
    type = "ssh"
    host = aws_eip.serverip.public_ip
    user = "ubuntu"
    private_key = file(local_file.prkey.filename)
  }
}

resource "null_resource" "fetch_node_token" {
  depends_on = [module.k3s]

  provisioner "remote-exec" {
    inline = ["sh -c -- \"sudo cp /var/lib/rancher/k3s/server/node-token /tmp/node-token; sudo chmod a+r /tmp/node-token\""]
  }

  provisioner "local-exec" {
    command = "scp -i ${local_file.prkey.filename} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${aws_eip.serverip.public_ip}:/tmp/node-token node-token"
  }

  provisioner "remote-exec" {
    inline = ["sh -c -- \"sudo rm -f /tmp/node-token\""]
  }

  connection {
    type = "ssh"
    host = aws_eip.serverip.public_ip
    user = "ubuntu"
    private_key = file(local_file.prkey.filename)
  }
}

data "local_file" "node_token" {
  depends_on = [null_resource.fetch_node_token]
  filename = "node-token"
}