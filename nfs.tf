
resource "helm_release" "nfscsi" {
  depends_on = [null_resource.fetch_kubeconfig]

  repository = "https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/charts"
  chart = "csi-driver-nfs"
  name  = "csi-driver-nfs"
  namespace = "kube-system"
  atomic = true
  timeout = 45
}

resource "kubernetes_storage_class" "nfsstorageclass" {
  depends_on = [helm_release.nfscsi]
  
  metadata {
    name = "nfs-csi"
    annotations = {
      "storageclass.kubernetes.io/is-default-class" = "true"
    }
  }
  
  storage_provisioner = "nfs.csi.k8s.io"
  reclaim_policy = "Delete"
  volume_binding_mode = "Immediate"
  parameters = {
    server = aws_efs_file_system.efs.dns_name
    share = "/"
  }
  
  mount_options = [
    "nfsvers=4.1",
    "rsize=1048576",
    "wsize=1048576",
    "hard",
    "timeo=600",
    "retrans=2",
    "noresvport"
  ]
}