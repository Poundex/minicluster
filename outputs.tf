
output "priv" {
  value = "${local.private_a} ${local.private_b} ${local.private_c}"
}

output "pub" {
  value = "${local.public_a} ${local.public_b} ${local.public_c}"
}