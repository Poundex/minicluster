
#provider "aws" {}

terraform {
  required_providers {
    aws = {}
    helm = {}
    acme = {
      source  = "vancluever/acme"
      version = "~> 2.5.3"
    }
  }
}

provider "kubernetes" {
  config_path = "kubeconfig"
}
#
#provider "helm" {
#  kubernetes {
#    config_path = "kubeconfig"
#  }
#}

provider "acme" {
  ## Live provider has limit 5 issues over 7 days, use staging for testing

  ## STAGING PROVIDER
#    server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"

  ##LIVE PROVIDER
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}