variable "myip" {
  type = string
}
#variable "dns_zone_id" {
#  type = string
#}
#
#variable "dns_hostname" {
#  type = string
#}
#
#variable "cert_email" {
#  type = string
#}
#
variable "env_name" {
  type = string
}

variable "vpc_root_space" {
  type = string
}

variable "aws_region" {
  type = string
  default = "eu-west-2"
}

variable "aws_azs" {
  type = list(string)
  default = ["a", "b", "c"]
}

variable "k8s_node_count" {
  type = number
}

variable "hosted_zone_id" {
  type = string
}

variable "k8s_server_subdomain" {
  type = string
  default = "k8s"
}

variable "root_domain" {
  type = string
}

variable "flux_git_url" {
  type = string
}

variable "flux_git_username" {
  type = string
}

variable "flux_git_password" {
  type = string
}

variable "cert_email" {
  type = string
}

variable "k8s_node_instance_type" {
  type = string
  default = "t3a.large"
}