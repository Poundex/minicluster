
module "k8s_server" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "4.0.0"

  name = "${var.env_name}-k8s-server"

  ami                    = "ami-0510e632102e49c91"
  instance_type          = "t3a.small"
  key_name               = aws_key_pair.awskeypair.key_name
  monitoring             = false
  vpc_security_group_ids = [module.vpc.default_security_group_id, module.sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  cpu_credits = "standard"

  create_spot_instance = false

  tags = {
    Terraform   = "true"
    Environment = var.env_name
  }
}

resource "aws_eip" "serverip" {
  instance = module.k8s_server.id
  vpc      = true
}

resource "aws_eip" "nodeips" {
#  count = var.k8s_node_count
  count = 0 
  
  instance = module.k8s_nodes[count.index].spot_instance_id
  vpc      = true
}

resource "null_resource" "server_node_tidy" {
  depends_on = [module.k8s_server, aws_eip.serverip]

  provisioner "remote-exec" {
    inline = [
      "sudo systemctl stop snapd",
      "sudo apt remove --purge --assume-yes snapd"
    ]
  }
  connection {
    type        = "ssh"
    host        = aws_eip.serverip.public_ip
    user        = "ubuntu"
    private_key = file(local_file.prkey.filename)
  }
}


module "k8s_nodes" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "4.0.0"
#  count = var.k8s_node_count
  count = 0 
  depends_on = [null_resource.fetch_node_token]

  name = "${var.env_name}-k8s-node-${count.index}"

  ami                    = "ami-0510e632102e49c91"
  instance_type          = var.k8s_node_instance_type
  key_name               = aws_key_pair.awskeypair.key_name
  monitoring             = false
  vpc_security_group_ids = [module.vpc.default_security_group_id, module.sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[count.index % length(module.vpc.public_subnets)]
  cpu_credits = "standard"

  create_spot_instance = true
  
  user_data = <<USERDATA
#!/usr/bin/env bash
curl -sfL https://get.k3s.io | K3S_URL=https://${module.k8s_server.private_ip}:6443 K3S_TOKEN="${trimspace(data.local_file.node_token.content)}" sh -
USERDATA

  root_block_device = [
    {
      volume_size = 16
    },
  ]

  tags = {
    Terraform   = "true"
    Environment = var.env_name
  }
}

resource "null_resource" "nodes_tidy" {
  depends_on = [module.k8s_nodes, aws_eip.nodeips]
  
  count = var.k8s_node_count

  provisioner "remote-exec" {
    inline = [
      "sudo systemctl stop snapd",
      "sudo apt remove --purge --assume-yes snapd"
    ]
  }
  connection {
    type        = "ssh"
    host        = aws_eip.nodeips[count.index].public_ip
    user        = "ubuntu"
    private_key = file(local_file.prkey.filename)
  }
}

module "nodeasg" {
  source     = "terraform-aws-modules/autoscaling/aws"
  depends_on = [null_resource.fetch_node_token]
  count = 1 

  name    = "${var.env_name}-asg"
  version = "6.9.0"

  min_size                  = 1
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = "30s"
  default_cooldown = 0

  key_name            = aws_key_pair.awskeypair.key_name
  security_groups     = [module.vpc.default_security_group_id, module.sg.security_group_id]
  vpc_zone_identifier = [module.vpc.public_subnets[count.index % length(module.vpc.public_subnets)]]
  enable_monitoring   = false
  image_id = "ami-0510e632102e49c91"

  user_data = base64encode(<<USERDATA
#!/usr/bin/env bash
sudo systemctl stop snapd
sudo apt remove --purge --assume-yes snapd
curl -sfL https://get.k3s.io | K3S_URL=https://${module.k8s_server.private_ip}:6443 K3S_TOKEN="${trimspace(data.local_file.node_token.content)}" sh -
USERDATA
    )

  use_mixed_instances_policy = true
  mixed_instances_policy = {
    instances_distribution = {
      on_demand_base_capacity                  = 0
      on_demand_percentage_above_base_capacity = 0
      spot_allocation_strategy                 = "lowest-price"
    }

    override = [
      {
        instance_type = "m5a.large"
      },
      {
        instance_type = "m5.large"
      },
      {
        instance_type = "t3a.large"
      },
      {
        instance_type = "t3.large"
      },
    ]
  }
  
  block_device_mappings = [
    {
      device_name = "/dev/sda1"
      ebs         = {
        delete_on_termination = true
        encrypted             = false
        volume_size           = 16
        volume_type           = "gp2"
      }
    }
  ]
}