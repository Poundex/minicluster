

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "registration" {
  account_key_pem = tls_private_key.private_key.private_key_pem
  email_address   = var.cert_email
}

resource "acme_certificate" "certificate" {
  account_key_pem           = acme_registration.registration.account_key_pem
  common_name               = var.root_domain
  subject_alternative_names = ["*.${var.root_domain}"]
  
  dns_challenge {
    provider = "route53"

    config = {
      AWS_HOSTED_ZONE_ID = var.hosted_zone_id
      AWS_PROFILE = "pxpx"
    }
  }

  depends_on = [acme_registration.registration]
}

resource "local_file" "certificate_pem" {
  filename = "cert.crt"
  content = lookup(acme_certificate.certificate, "certificate_pem")
}

resource "local_file" "issuer_pem" {
  filename = "issuer.crt"
  content = lookup(acme_certificate.certificate, "issuer_pem")
}

resource "local_file" "key_pem" {
  filename = "key.pem"
  content = nonsensitive(lookup(acme_certificate.certificate, "private_key_pem"))
}

