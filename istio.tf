
resource "kubernetes_namespace" "istio_ns" {
  depends_on = [null_resource.fetch_kubeconfig]
  
  metadata {
    name = "istio-system"
  }
}

resource "helm_release" "istio_base" {
  depends_on = [kubernetes_namespace.istio_ns]
  
  repository = "https://istio-release.storage.googleapis.com/charts"
  chart = "base"
  name  = "istio-base"
  namespace = "istio-system"
  atomic = true
  timeout = 45
}

resource "helm_release" "istiod" {
  depends_on = [helm_release.istio_base]
  
  repository = "https://istio-release.storage.googleapis.com/charts"
  chart = "istiod"
  name  = "istiod"
  namespace = "istio-system"
  atomic = true
  timeout = 45
  
  set {
    name  = "global.proxy.resources.requests.cpu"
    value = "1m"
  }
  
#  set {
#    name  = "meshConfig.accessLogFile"
#    value = "/dev/stdout"
#  }
  
}

resource "kubernetes_namespace" "istioingns" {
  metadata {
    name = "istio-ingress"
    labels = {
      istio-injection = "enabled"
    }
  }
}

resource "helm_release" "istioing" {
  depends_on = [kubernetes_namespace.istioingns, helm_release.istiod]

  repository = "https://istio-release.storage.googleapis.com/charts"
  chart = "gateway"
  name  = "istio-ingress"
  namespace = "istio-ingress"
  atomic = true
  timeout = 60
}

resource "helm_release" "istioingnor" {
  depends_on = [kubernetes_namespace.istioingns]

  repository = "https://gitlab.com/api/v4/projects/39438431/packages/helm/stable"
  chart = "istio-ingress-normaliser"
  name  = "ingress-normaliser"
  namespace = "istio-ingress"
  atomic = true
  timeout = 60
  
  version = "0.3.1"
  
  set {
    name  = "ingress.credential"
    value = "cert"
  }
  
  set {
    name  = "ingress.singleGateway"
    value = "istio-ingress/gateway"
  }
}

resource "kubernetes_secret" "certsecret" {
  depends_on = [kubernetes_namespace.istioingns, acme_certificate.certificate, local_file.certificate_pem, local_file.key_pem]
  
  metadata {
    name = "cert"
    namespace = "istio-ingress"
  }
  
  data = {
    "tls.crt" = "${local_file.certificate_pem.content}${local_file.issuer_pem.content}"
    "tls.key" = local_file.key_pem.content
  }
}

resource "kubernetes_manifest" "gateway" {

  depends_on = [helm_release.istioing]
  
  manifest = {
    apiVersion = "networking.istio.io/v1beta1"
    kind = "Gateway"

    metadata = {
      name = "gateway"
      namespace = "istio-ingress"
    }

    spec = {
      selector = {
        istio = "ingress"
      }
      servers = [
        {
          hosts = ["*.${var.root_domain}"]
          port = {
            name = "https"
            number = 443
            protocol = "HTTPS"
          }
          tls = {
            credentialName = "cert"
            mode = "SIMPLE"
          }
        }
      ]
    }
  }
}
