locals {
  vpc_cidr = "${var.vpc_root_space}/16"
  
  private_a = cidrsubnet(local.vpc_cidr, 8, 1)
  private_b = cidrsubnet(local.vpc_cidr, 8, 2)
  private_c = cidrsubnet(local.vpc_cidr, 8, 3)
  
  public_a = cidrsubnet(local.vpc_cidr, 8, 101) 
  public_b = cidrsubnet(local.vpc_cidr, 8, 102)
  public_c = cidrsubnet(local.vpc_cidr, 8, 103)
  
  region_azs = [for s in var.aws_azs : "${var.aws_region}${s}"]
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.14.2"

  name = var.env_name
  cidr = local.vpc_cidr

  azs             = local.region_azs
  private_subnets = [local.private_a, local.private_b, local.private_c]
  public_subnets  = [local.public_a, local.public_b, local.public_c]

  enable_nat_gateway = false
  enable_vpn_gateway = false
  
  enable_dns_hostnames = true
  enable_dns_support = true 

  tags = {
    Terraform = "true"
    Environment = var.env_name
  }
}

module "sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "segrme"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      rule        = "all-all"
      cidr_blocks = "${var.myip}/32"
    }
  ]
  egress_with_cidr_blocks = [
    {
      rule = "all-all",
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}