
locals {
  k8s_server_domain = "${var.k8s_server_subdomain}.${var.root_domain}"
}

resource "aws_route53_record" "k8s_server_dns" {
  name    = local.k8s_server_domain
  type    = "A"
  zone_id = var.hosted_zone_id
  ttl = 30
  
  records = [aws_eip.serverip.public_ip]
}

resource "aws_route53_record" "cluster_dns" {
  name    = "*.${var.root_domain}"
  type    = "A"
  zone_id = var.hosted_zone_id
  ttl = 600
  
#  records = [for n in aws_eip.nodeips: n.public_ip]
#  multivalue_answer_routing_policy = true
  records = ["13.41.33.77"]
}
