resource "aws_iam_user" "iamuser" {
  name = "secretsreader"

  tags = {
    Environment = var.env_name
  }
}

resource "aws_iam_access_key" "iamkey" {
  user = aws_iam_user.iamuser.name
}

resource "aws_iam_user_policy" "iampolicy" {
  name = "test"
  user = aws_iam_user.iamuser.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "secretsmanager:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "kubernetes_namespace" "xsecretns" {
  metadata {
    name = "external-secrets"
    labels = {
      istio-injection = "enabled"
    }
  }
}

resource "helm_release" "xsecret" {
  depends_on = [helm_release.istiod, kubernetes_namespace.xsecretns]

  repository = "https://charts.external-secrets.io"
  chart = "external-secrets"
  name  = "external-secrets"
  namespace = "external-secrets"
  atomic = true
  timeout = 300
}

resource "kubernetes_secret" "awssmsecret" {
  metadata {
    name = "aws"
    namespace = "external-secrets"
  }
  
  data = {
    access = aws_iam_access_key.iamkey.id
    secret = aws_iam_access_key.iamkey.secret
  }
}

resource "kubernetes_manifest" "secretstore" {
  
  count = 1

  depends_on = [helm_release.xsecret, kubernetes_namespace.xsecretns]
  
  manifest = {
    apiVersion = "external-secrets.io/v1beta1"
    kind = "ClusterSecretStore"
    
    metadata = {
      name = "aws"
    }
    
    spec = {
      provider = {
        aws = {
          service = "SecretsManager"
          region = var.aws_region
          auth = {
            secretRef = {
              accessKeyIDSecretRef = {
                name = "aws"
                key = "access"
                namespace = "external-secrets"
              }
              secretAccessKeySecretRef = {
                name = "aws"
                key = "secret"
                namespace = "external-secrets"
              }
            }
          }
        }
      }
    }
  }
}