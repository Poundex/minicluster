
resource "aws_efs_file_system" "efs" {
  encrypted = false
  
  tags = {
    Name = var.env_name
    Environment = var.env_name
  }
}

resource "aws_efs_backup_policy" "efsbackup" {
  file_system_id = aws_efs_file_system.efs.id
  backup_policy {
    status = "DISABLED"
  }
}

resource "aws_efs_mount_target" "efsmnt" {
  count = length(module.vpc.private_subnets)
  
  file_system_id = aws_efs_file_system.efs.id
  subnet_id      = module.vpc.private_subnets[count.index]
  security_groups = [module.vpc.default_security_group_id]
}

