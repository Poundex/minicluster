
resource "kubernetes_namespace" "flux_ns" {
  depends_on = [null_resource.fetch_kubeconfig]
  
  metadata {
    name = "flux-system"
  }
}

resource "helm_release" "flux" {
  depends_on = [kubernetes_namespace.istio_ns]
  
  repository = "https://fluxcd-community.github.io/helm-charts"
  chart = "flux2"
  name  = "flux"
  namespace = "flux-system"
  atomic = true
  timeout = 60
  
  set {
    name  = "notificationcontroller.create"
    value = "false"
  }
}

resource "helm_release" "fluxsync" {
  depends_on = [kubernetes_namespace.istio_ns, helm_release.flux, kubernetes_secret.kres_flux_git_secret]

  repository = "https://fluxcd-community.github.io/helm-charts"
  chart = "flux2-sync"
  name  = "fluxsync"
  namespace = "flux-system"
  atomic = true
  timeout = 60

  set {
    name  = "gitRepository.spec.url"
    value = var.flux_git_url
  }
  
  set {
    name  = "gitRepository.spec.secretRef.name"
    value = "flux-git-secret"
  }
  
  set {
    name  = "kustomization.spec.path"
    value = "deployments/"
  }
  
  set {
    name  = "gitRepository.spec.interval"
    value = "2m"
  }
  
  set {
    name  = "kustomization.spec.interval"
    value = "2m"
  }
}

resource "kubernetes_secret" "kres_flux_git_secret" {
  depends_on = [kubernetes_namespace.flux_ns]
  
  metadata {
    namespace = "flux-system"
    name = "flux-git-secret"
  }
  
  data = {
    username = var.flux_git_username
    password = var.flux_git_password
  }
}