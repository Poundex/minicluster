
resource "tls_private_key" "keypair" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "pkey" {
  filename = "id_rsa.pub"
  content = tls_private_key.keypair.public_key_pem
}

resource "local_file" "prkey" {
  filename = "id_rsa"
  content = tls_private_key.keypair.private_key_pem
}

resource "aws_key_pair" "awskeypair" {
  key_name   = var.env_name
  public_key = tls_private_key.keypair.public_key_openssh
  
  tags = {
    Environment = var.env_name
  }
}

resource "null_resource" "key_perms" {
  depends_on = [local_file.prkey]
  
  provisioner "local-exec" {
    command = "chmod 600 id_rsa"
  }
}
